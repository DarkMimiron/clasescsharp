﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VariablesConstantes {
    class ClassLevelScope {

        int abc = 1000; // variable de "class level" con nivel de clase

        public void Mostrar() {
            Console.WriteLine(abc); // metodo para acceder a la variable de nivel de clase 
        }
    }
}
