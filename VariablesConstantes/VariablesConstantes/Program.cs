﻿using System;

namespace VariablesConstantes {
    class Program {
        static void Main(string[] args) {

            /* Declaracion de variables */
            int variable;               // Declaracion de variable

            int Variable = 10;          // Declaracion y asignacion de variable

            const float Pi = 3.1416f;   // Declaracion y asignacion de constante

            // Console.WriteLine(variable); Error porque la variable no esta asignada
            Console.WriteLine(Variable);    // 10
            Console.WriteLine(Pi);          // 3.1416


            /* Convenciones de nombres */
            int primerNumero = 2;            // Camel Case
            const int PrimerNumero = 2842;      // Pascal Case

            Console.WriteLine(primerNumero);
            Console.WriteLine(PrimerNumero);


            /* Tipos primitivos */
            byte numeroPequeno = 255;           // Equivalente a Byte en .NET

            short numeroCorto = -32768;         // Equivalente a Int16 en .NET

            char letra = 'F';                   // Equivalente a Char en .NET. Debe ser asignada con
                                                // comillas simples

            float numeroFlotante = 32.512f;     // Si no se coloca una 'f' al final, el compilador
                                                // lo interpreta como double

            decimal numeroDecimal = 853.7535m;  // Si no se coloca una 'm' al final, el compilador
                                                // lo interpreta como double

            Console.WriteLine(numeroPequeno);
            Console.WriteLine(numeroCorto);
            Console.WriteLine(letra);
            Console.WriteLine(numeroFlotante);
            Console.WriteLine(numeroDecimal);


            /* Method-Level Scope */
            int calif;              // Declarada a nivel de metodo
            calif = 100;            // Usada en method scope
            if (calif >= 70) {
                // Usada en nested scope
                Console.WriteLine("Calificacion aprobatoria: {0}", calif);
            } else {
                // Usada de nuevo en nested scope
                Console.WriteLine("Calificacion reprobatoria: {0}", calif);
            }

            /* Nested Scope */
            int puntuacion = 100;
            if (puntuacion >= 60) {
                string mensaje = "Buena puntuacion";    // Declarada en el if
            } else {
                string mensaje = "Mala puntuacion";     // Declarada en el else
            }
            // Console.WriteLine(mensaje);              Variable no disponible
        }
    }
}
