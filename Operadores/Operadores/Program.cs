﻿using System;

namespace Operadores {
    class Program {
        static void Main(string[] args) {
            /* Operadores aritmeticos */
            int a = 10;
            int b = 20;
            int c;

            Console.WriteLine(a + b);   // Operador de suma
            Console.WriteLine(a - b);   // Operador de resta
            Console.WriteLine(a * b);   // Operador de multiplicacion
            Console.WriteLine(b / a);   // Operador de division
            Console.WriteLine(b % a);   // Operador de modulo

            a++;    // Operador de incremento
            b--;    // Operador de sustraccion
            Console.WriteLine(a);
            Console.WriteLine(b);

            c = a++;
            Console.WriteLine(c);
            Console.WriteLine(a);

            /* Operadores de comparacion */
            Console.WriteLine(a == b);      // Igual (comparacion)
            Console.WriteLine(a != b);      // No es igual
            Console.WriteLine(a > b);       // Mayor que
            Console.WriteLine(a < b);       // Menor que
            Console.WriteLine(a >= b);      // Mayor o igual que
            Console.WriteLine(a <= b);      // Menor igual que

            /* Operadores de asignacion */
            int num1 = 10;                  // Asignacion
            Console.WriteLine(num1);        // 10

            int num2 = 5;               
            num2 += 10;                     // Asignacion de suma
            Console.WriteLine(num2);        // 15

            num2 -= 5;                      // Asignacion de resta
            Console.WriteLine(num2);        // 10

            num1 *= 20;                     // Asignacion de multiplicacion
            Console.WriteLine(num1);        // 200

            num1 /= 10;                     // Asignacion de division
            Console.WriteLine(num1);        // 20

            /* Operadores de logicos */
            bool verdad = true;
            bool mentira = false;

            Console.WriteLine(verdad && verdad);        // Operador logico AND
            Console.WriteLine(mentira || verdad);       // Operador logico OR
            Console.WriteLine(!mentira);                // Operador logico NOT
        }
    }
}
